#include "file/file.h"
#include "bmp/bmp.h"
#include "img/image.h"
#include "transform/rotate.h"
#include <stdio.h>


int main(int argc, char **argv) {
    (void) argc;
    (void) argv; // suppress 'unused parameters' warning

    if (argc < 3) {
        fprintf(stderr, "Usage: ./image-transformer <source-image> <transformed-image>");
        return -1;
    }

    FILE *input_bmp = open_file(argv[1], "r");
    struct image *original_image = alloc_image(0, 0);
    from_bmp(input_bmp, original_image);
    close_file(input_bmp);

    FILE *output_bmp = open_file(argv[2], "w");
    struct image *rotated_image = rotate_image(original_image);
    to_bmp(output_bmp, rotated_image);
    close_file(output_bmp);

    free_image(original_image);
    free_image(rotated_image);

    return 0;
}
